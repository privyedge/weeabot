# weeabot

A Matrix bot written in Rust

## Features

This bot is designed to interact with a chatroom using 
a host of functions ranging from helpful language tricks 
and fun reactions to various triggers.

This bot makes use of the
[matrix_bot_api crate](https://docs.rs/matrix_bot_api/0.4.0/matrix_bot_api/)